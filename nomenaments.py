#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import time
import urllib3
from lxml import html

TODAY = time.strftime("%Y%m%d")
TODAY_FORMAT = time.strftime("%d/%m/%Y")
FILE_NAME = '/var/www/sindicat.net/htdocs/profint/nomenaments/llegir/' + TODAY + '.php'
#FILE_NAME = TODAY + '-test' + '.html'
SERVEIS_TERRITORIALS_FILE = os.path.join(
                                         os.path.dirname(os.path.realpath(__file__)),
                                         'serveis-territorials')

def fill_tree(st_url_code, nivell):
    # Build url
    url = 'https://aplicacions.ensenyament.gencat.cat/e13_bor/consultaAdjudicacionsEsp.do?id=' + st_url_code + '&nivell=' + nivell
    # Get html page
    response = urllib3.PoolManager()
    html_content = response.request('GET', url).data
    # Convert text to html tree
    tree = html.fromstring(html_content)
    return tree

def fill_nomenaments(st_name, especialitats, convocats, nomenaments):
    i=0
    for e in especialitats:
        # Get especialitat and convocat number
        especialitat = e.strip().rstrip().split()[1]
        convocat = convocats[i].strip().rstrip()
        # Add convocat number for especialitat in this servei terrirorial dict
        # to main dict of nomenaments
        if convocat != '':
            nomenaments[st_name][especialitat] = convocat
        i = i + 1

def get_convocat(st, especialitat, nomenaments):
    # Check if there is servei territorial dict
    if st in nomenaments:
        # Check if there is any convocat for the especialitat
        if especialitat in nomenaments[st]:
            # Get convocat number
            n = nomenaments[st][especialitat]
            if n == 'No hi ha convocats':
                return '-'
            else:
                return nomenaments[st][especialitat]
    return ''

def get_especialitats(nomenaments):
    # Create a set with all especialitats
    especialitats = set()
    # Loop nomenaments
    for st, esps in nomenaments.items():
        # For each servei territorial, loop especialitats
        # and add them to the set
        for e in esps.keys():
            especialitats.add(e)
    return especialitats

def get_data_nomenaments(tree):
    # Get date
    data_nomenaments = tree.xpath('//div[@id="panel_0_0"]/div/div/h4[3]/text()')
    # Ignore HTML entites (blank spaces)
    data_nomenaments = data_nomenaments[0].strip().rstrip()
    # If theres is a date, get it. If not, return ''
    if data_nomenaments != '':
        # String format is Data:dd/mm/yyyy. Get only dd/mm/yyyy
        data_nomenaments = data_nomenaments.split(':')[1].strip().rstrip()
    return data_nomenaments

def build_html_table(st_names, especialitats_primaria, especialitats_primaria_secundaria, especialitats_secundaria,
                     nomenaments_primaria, nomenaments_primaria_secundaria, nomenaments_secundaria):
    nomenaments_table = open(FILE_NAME, 'w')
    nomenaments_table.write('<table border=\'1\' class=\'taula\'>')
    # Header row with serveis territorials
    nomenaments_table.write('<tr>')
    nomenaments_table.write('<td></td>')
    for item in st_names:
        nomenaments_table.write('<td style="font-size:11px; text-align:center;">' +
                                item + '</td>')
    nomenaments_table.write('</tr>')
    # Row with date
    nomenaments_table.write('<tr class="fonsverdclar">')
    nomenaments_table.write('<td style="font-size:11px; text-align:center;">DATA:</td>')
    for st in st_names:
        nomenaments_table.write('<td style="font-size:11px; text-align:center;">')
        nomenaments_table.write(TODAY_FORMAT)
        nomenaments_table.write('</td>')
    nomenaments_table.write('</tr>')
    # Rows with primaria specialities and primaria with secundaria teachers
    build_convocats_row(nomenaments_table, nomenaments_primaria, nomenaments_primaria_secundaria, especialitats_primaria, st_names)
    # Check if there is some speciality where secondary teachers have been called and primaria teachers don't
    diff = especialitats_primaria_secundaria - especialitats_primaria
    build_convocats_row(nomenaments_table, nomenaments_primaria_secundaria, None, diff, st_names)
    # Rows with secundaria specialities
    build_convocats_row(nomenaments_table, nomenaments_secundaria, None, especialitats_secundaria, st_names)
    nomenaments_table.write('</table>')

def build_convocats_row(nomenaments_table, nomenaments1, nomenaments2, especialitats, st_names):
    # Sort putting letters before numbers
    for e in sorted(especialitats, key=lambda x: (x[0].isdigit(), x)):
        nomenaments_table.write('<tr>')
        # Write especialitat
        nomenaments_table.write('<td style="font-size:11px; text-align:center;">' + e + '</td>')
        # For each servei territorial, write convocat
        for st in st_names:
            c = get_convocat(st, e, nomenaments1)
            nomenaments_table.write('<td style="font-size:11px; text-align:center;">' + c)
            # If there is another dict of nomenaments it means that we have teachers
            # of secundaria that can be called for primaria
            if nomenaments2 is not None and c != '':
                nomenaments_table.write(' s:' + get_convocat(st, e, nomenaments2))
            nomenaments_table.write('</td>')
        nomenaments_table.write('</tr>')

def main():
    # Build a dict of dicts for  each kind of nomenament:
    # the first key is the st, and the value is a dict with
    # all especialitats and its convocat number
    # nomenaments={'Barcelona': {'507': '1', '627': '2'},
    #              'Girona': {'507': '1', '627': '2'}
    #             }
    # Init main dict
    nomenaments_primaria = {}
    nomenaments_primaria_secundaria = {}
    nomenaments_secundaria = {}
    # Traverse st file
    st = open(SERVEIS_TERRITORIALS_FILE, 'r')
    # Init list to save serveis territorials names
    st_names = []
    for line in st:
        # Get st values removing newline
        st_values = line[:-1].split(',')
        # Get servei territorial name
        st_name = st_values[0]
        # Save serveis territorials names in a list
        st_names.append(st_name)
        # Get servei territorial url code
        st_url_code = st_values[1]
        # Create a dict for each territorial inside nomenaments dict
        nomenaments_primaria[st_name] = {}
        nomenaments_primaria_secundaria[st_name] = {}
        nomenaments_secundaria[st_name] = {}

        # PRIMARIA
        # Get primaria page
        tree = fill_tree(st_url_code, 'P')
        # Get date
        data_nomenaments = get_data_nomenaments(tree)
        if data_nomenaments != '' and data_nomenaments == TODAY_FORMAT:
            # Get table data for nomenaments of primaria
            especialitats = tree.xpath('//div[@id="fw_colCos"]/table[1]/tr/td[1]/text()')
            convocats = tree.xpath('//div[@id="fw_colCos"]/table[1]/tr/td[2]/text()')
            # Fill dict with data
            fill_nomenaments(st_name, especialitats, convocats, nomenaments_primaria)
            # Get table data for nomenaments of primaria (for secundaria teachers)
            especialitats = tree.xpath('//  div[@id="fw_colCos"]/table[2]/tr/td[1]/text()')
            convocats = tree.xpath('//div[@id="fw_colCos"]/table[2]/tr/td[2]/text()')
            # Fill dict with data
            fill_nomenaments(st_name, especialitats, convocats, nomenaments_primaria_secundaria)
        # SECUNDARIA
        # Get secundaria page
        tree = fill_tree(st_url_code, 'S')
        # Get date
        data_nomenaments = get_data_nomenaments(tree)
        if data_nomenaments != '' and data_nomenaments == TODAY_FORMAT:
            # Get table data for nomenaments of secundaria
            especialitats = tree.xpath('//div[@id="fw_colCos"]/table/tr/td[1]/text()')
            convocats = tree.xpath('//div[@id="fw_colCos"]/table/tr/td[2]/text()')
            # Fill dict with data
            fill_nomenaments(st_name, especialitats, convocats, nomenaments_secundaria)

    # Create a list with convocated especialitats
    especialitats_primaria = get_especialitats(nomenaments_primaria)
    especialitats_primaria_secundaria = get_especialitats(nomenaments_primaria_secundaria)
    especialitats_secundaria = get_especialitats(nomenaments_secundaria)

    # Create HTML table
    build_html_table(st_names, especialitats_primaria, especialitats_primaria_secundaria, especialitats_secundaria,
                               nomenaments_primaria, nomenaments_primaria_secundaria, nomenaments_secundaria)

if __name__ == "__main__":
    main()
